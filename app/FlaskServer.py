#!/usr/bin/env python3

from flask import Flask, render_template, Response
#from flask_socketio import SocketIO, emit
from .Monitor import EnvironmentMonitor
from matplotlib import pyplot as plt
from matplotlib import animation
import numpy as np
import pandas as pd
import time
import itertools


app = Flask(__name__, template_folder='templates')
#app.config["SECRET_KEY"] = "ThisIsSecret!"
#socketio = SocketIO(app)

@app.route("/")
def index():
    return render_template("Presentation.html")

def graphs(measurement, roundtime, run):
    # the code is written by the inspiration of the following site:
    # https://learn.sparkfun.com/tutorials/graph-sensor-data-with-python-and-matplotlib/speeding-up-the-plot-animation
    fig, ax = plt.subplots()
    plt.xlim(0, roundtime)
    #line, = ax.plot([], [], lw=2)
    #fig = plt.figure()
    e = EnvironmentMonitor()
    _initial = pd.Series(
            index=np.arange(0,roundtime),
            data=list(
                itertools.repeat(
                    0, 
                    roundtime
                    )
                )
            )
    try:
        _initial = next(
            e.data(
                measurement, 
                roundtime
                )
            )
    except StopIteration:
        if "Test" in measurement:
            _initial.loc[0] = np.random.randint(
                -10, 
                30, 
                size=1
                )[0]

    def init():
        #return ax.plot([], []),
        return plt.plot(_initial.index, _initial.values)
    
    if "Temperature" in measurement:
        plt.title("Temperature over Time")
        plt.xlabel("Time")
        plt.ylabel("Temperature [°C]")
        plt.ylim(-10, 30)

    if "Test" in measurement:
        plt.ylim(-10, 30)

    def animate(i):

        #return ax.plot(i.index.time, i.values)
        #plt.xticks(i.index.time)
        return plt.plot(i.index, i.values)

    def frames():
        if "Temperature" in measurement:
            d = pd.Series()
            for r in e.data(measurement, roundtime):
                r.index = r.index.to_pydatetime()
                d = pd.concat([d, r], axis=0)
                if len(d.index) > roundtime:
                    d = d.iloc[1:]
                yield d
        if "Test" in measurement:
            d = pd.Series()
            i = 0
            while True:
                d.loc[i] = np.random.randint(
                    -10, 
                    30, 
                    size=1
                    )[0]
                print(d)
                i += 1
                if len(d.index) > roundtime:
                    d = d.iloc[1:]
                    d.index = np.around(
                        np.arange(
                            0, 
                            len(d.index)
                            ), 
                        decimals=0
                    )
                time.sleep(0.99)
                yield d
            
    if run is True:
        anim = animation.FuncAnimation(
            fig, 
            animate,
            init_func=init,
            frames=frames(), 
            interval=400,
            blit=True
            )
        #anim.to_html5_video()
        #anim.to_jshtml()
        # solution by:
        # http://louistiao.me/posts/notebooks/embedding-matplotlib-animations-in-jupyter-as-interactive-javascript-widgets/
        #HTML(anim.to_html5_video())
        #plt.show()
        #anim.save("Test.mp4")

# please give matplotlib to JS by the mpld3 a try

#@socketio.on("temp", namespace="/temp_input")
#def data_input(message):
#    e = EnvironmentMonitor()
#    for d in e.data(measurement="Temperature", round_time=200):
#        emit("temp", {d.index[0]: d.values[0]})


@app.route("/temp")
# these values have to match, otherwise the mapping to the url fails
def temp(roundtime=200):
    # the control structure follows these pages:
    # https://docs.python.org/3.6/library/threading.html#thread-objects
    # and 
    # https://www.shanelynn.ie/using-python-threading-for-multiple-results-queue/
    #measurement_methods = ["Temperature", "Ammonia"]
    #for m in measurement_methods:
    #    process = Thread(target=, args=[m, roundtime])
    #    process.start()
    t = graphs(
            measurement="Temperature", 
            roundtime=roundtime, 
            run=True
        )
    time.sleep(2)
    return Response(t)

@app.route("/test")
def test():
    t = graphs(
        measurement="Test",
        roundtime=200,
        run=True
        )
    return t
    


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
    #socketio.run(app)