#!/usr/bin/python3

from .TempLogger import TempLogger
from matplotlib import pyplot as plt
from matplotlib import animation
import pandas as pd


templogger = TempLogger()

def frames():
    while True:
        for r in templogger.control_structure():
            r.index = r.index.to_pydatetime()
            yield r

fig = plt.figure()

def animate(i):
    plt.cla()
    # this clears the plot, please refer to:
    # https://stackoverflow.com/questions/8213522/when-to-use-cla-clf-or-close-for-clearing-a-plot-in-matplotlib#8228808
    return plt.plot(i.index, i.values, color="r")

anim = animation.FuncAnimation(
    fig, 
    animate, 
    frames=frames, 
    interval=1000,
    blit=True
    )
anim.to_html5_video()
plt.show()