#!/usr/bin/env python3


from .TempLogger import TempLogger
import numpy as np
# package dependencies: libblas3 and libatlas3-base
import pandas as pd


class EnvironmentMonitor():
    def __init__(self):
        self.templogger = TempLogger()
        # the general concept is to run a local REST-API, which is 
        # then requested by the JavaScript on the WebPage and delievers the
        # graph
        # the REST-API might be run by flask

    def data(self, measurement, round_time):
        if "Temperature" in measurement:
            # the sensor involved is a ds18b20 sensor 
            # (this is the small black transistor based sensor)
            # find a tutorial here: 
            # https://www.youtube.com/watch?v=aEnS0-Jy2vE
            # and here:
            # http://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/
            # the blue cage sensor is a DHT11 sensor, 
            # find the tutorial here:
            # https://www.raspberrypi-spy.co.uk/2017/09/dht11-temperature-and-humidity-sensor-raspberry-pi/
            for r in self.templogger.control_structure(roundlength=round_time):
                r.index = r.index.to_pydatetime()
                yield r
        elif "Ammonia" in measurement:
            # the sensor involved is a Alltec mq-2 sensor
        # documentation can be found here:
        # https://tutorials-raspberrypi.de/raspberry-pi-gas-sensor-mq2-konfigurieren-und-auslesen/
            pass


# presentation will be done with matplotlib and the following method:
# https://pythonprogramming.net/python-matplotlib-live-updating-graphs/

if __name__ == "__main__":
    fig, ax = plt.subplots()

    def animate(i):
        return ax.plot(i.index, i.values)

    e = EnvironmentMonitor()
    anim = animation.FuncAnimation(fig, animate, frames=e.data("Temperature", 200), interval=1000)
    anim.to_html5_video()