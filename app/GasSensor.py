#!/usr/bin/python3

from MCP3008 import pinvalues
import pandas as pd
import numpy as np

# http://www.netzmafia.de/skripten/hardware/RasPi/RasPi_SPI.html
# https://learn.adafruit.com/raspberry-pi-analog-to-digital-converters/mcp3008
class GasSensor():
    def __init__(self, roundlength=200):
        # self.mcp = Adafruit_MCP3008.MCP3008(
        #   clk=18, # this sets the pin for the clock pin of the MCP 3008 chip
        #   cs=25, # this sets the ChipSelect/Shutdown Input
        #   miso=23, # this sets the digital output
        #   mosi=24 # this sets the digital input
        #   )
        self.roundlength = roundlength
        self.r_clean_air = 9.83
        # resistance of sensor in clean air as taken from the 
        # data sheet
        self.measures = {
            "CO":[
                5.1, 
                1.5, 
                (np.log10(1.5) - np.log10(5.1))/(np.log10(10000) - np.log10(200))
                ],
            "CH4":[
                3, 
                0.7, 
                (np.log10(0.7) - np.log10(3))/(np.log10(10000) - np.log10(200))
                ],
            "C2H6O":[
                2.9, 
                0.69, 
                (np.log10(0.69) - np.log10(2.9))/(np.log10(10000) - np.log10(200))
                ]
            }
            # these values are derived from the datasheet of the MQ-2 sensor
            # and are individual to each gas to measure
        self.calib = 0

    def req_values(self):
        c = self.calibration()
        r = pd.DataFrame(columns=list(self.measures.keys()))
        while True:
            for x in pinvalues():
                # reads the values from channel 0, to which the gas sensor
                # is wired, the value must not be 0, as otherwise a division
                # by 0 occurs
                if x is not 0:
                    _cal = self.calibration(self.resistance(x))
                    # this step allows to continuosly calibrate the 
                    # sensor
                    for g, d in self.measures.items():
                        print(g, d)
                        res = self.concentration(x, d)
                        print(res/_cal)
                        #r.loc[g, pd.Timestamp.now(tz="UTC")] = res/_cal
                #if len(r.index) > self.roundlength:
                #    r = r.iloc[1:]
                #yield r

    def resistance(self, v):
        return 5 * (1023 - v)/v
        # the formula calculates the resistance of the sensor
        # by multiplying the board resistance with the width -
        # the received value divided by the received value

    def concentration(self, data, values):
        # values are the precomputed base line values for the gas
        # concentration
        return np.power(
            10, 
            (((
                np.log10(
                    self.r_clean_air
                ) - values[0])/values[1]) + values[2]
            )
        )

    def calibration(self, v=None):
        l = []
        if v is not None and len(l) < 40:
            l.append(v)
        elif len(l) == 40:
            self.calib = np.mean(np.array(l))/self.r_clean_air
            l.clear()
            # removes all items from the list
            return self.calib
            # updated the self.calib variable to allow continuous 
            # calibration
        elif v is None:
            i = 0
            for x in pinvalues(0.1):
                if x is not 0:
                    l.append(x)
                    i += 40
                    if i > 40:
                        break
            self.calib = np.mean(np.array(l))/self.r_clean_air
            # the resistance of the sensor is required
            # to calculate the current deviation
            l.clear()
            return self.calib
        else:
            return self.calib
            # if the calibration variable is not recalculated, the 
            # preset variable is returned

if __name__ == "__main__":
    try:
        print(GasSensor().calibration())
        #x = True
        #while x is True:
        #    for x in GasSensor().calibration():
        #        print(x)
    except KeyboardInterrupt:
        x = False
        print("\n Terminating")