#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd
import datetime


class TempLogger():
    def __init__(self):
        self.rootdir = "/sys/bus/w1/devices/"

    def findfile(self):
        folder = os.listdir(self.rootdir)
        dfolder = [
            f for f in folder if "bus_master" not in f and "00-880000000000" not in f
            ][0]
        return dfolder

    def request_temp(self):
        sensordir = self.findfile()
        path = self.rootdir + sensordir + "/w1_slave"
        while True:
            with open(path, "r", encoding="utf-8") as tempfile:
                for l in tempfile:
                    if "t=" in l:
                        _l = l.split(" ")[-1]
                        yield int(_l.split("=")[-1])/1000

    def control_structure(self, work=True, roundlength=200):
        while work is True:
            for r in self.request_temp():
                res = pd.Series()
                res[pd.Timestamp.now(tz="UTC")] = r
                #if len(res.index) > roundlength:
                    # this limits the size of the series
                    # by deleting the first item upon adding of 
                    # a new item
                #    res = res.iloc[1:]
                yield res
    

if __name__ == "__main__":
    try:
        for r in TempLogger().control_structure():
            print(r)
    except KeyboardInterrupt:
        print("Terminating")